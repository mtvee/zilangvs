﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.Text.Classification;

namespace ZiLang
{
    class ZiLangLanguage
    {
        #region Member Variables

        private List<string> _comments = new List<string>();
        private List<string> _quoted = new List<string>();
        private List<string> _numeric = new List<string>();
        private List<string> _keyWords = new List<string>();
        private List<string> _identiferTypes = new List<string>();
        private List<string> _custom = new List<string>();

        #endregion

        #region Properties

        public List<string> Comments
        {
            get { return _comments; }
        }
        public List<string> Quoted
        {
            get { return _quoted; }
        }
        public List<string> Numeric
        {
            get { return _numeric; }
        }
        public List<string> KeyWords
        {
            get { return _keyWords; }
        }
        public List<string> IdentifierTypes
        {
            get { return _identiferTypes; }
        }
        public List<string> Custom
        {
            get { return _custom; }
        }

        #endregion

        #region ctor

        public ZiLangLanguage()
        {
            Initialize();
        }

        #endregion

        #region Methods

        private void Initialize()
        {
            _custom.Add(@"\breturn\b");

            _comments.Add(";");

            _quoted.Add(@"([""'])(?:\\\1|.)*?\1");

            _numeric.Add(@"\b\d+\b");
            _numeric.Add(@"\>");
            _numeric.Add(@"\<");
            _numeric.Add(@"\<\>");
            _numeric.Add(@"\=");
            _numeric.Add(@"\=\=");

            _keyWords.Add(@"\bdef\b");
            _keyWords.Add(@"\bset\b");
            _keyWords.Add(@"\blet\b");
            _keyWords.Add(@"\bif\b");
            _keyWords.Add(@"\bfn\b");
            _keyWords.Add(@"\bbegin\b");
            _keyWords.Add(@"\bwhile\b");
            _keyWords.Add(@"\bcond\b");
            _keyWords.Add(@"\bquote\b");
            _keyWords.Add(@"\beval\b");

            _keyWords.Add(@"\bexpt\b");
            _keyWords.Add(@"\bmodulo\b");
            _keyWords.Add(@"\bsqrt\b");
            _keyWords.Add(@"\binc\b");
            _keyWords.Add(@"\bdec\b");
            _keyWords.Add(@"\bfloor\b");
            _keyWords.Add(@"\bceil\b");
            _keyWords.Add(@"\bln\b");
            _keyWords.Add(@"\blog10\b");
            _keyWords.Add(@"\brand\b");
            _keyWords.Add(@"\bsin\b");
            _keyWords.Add(@"\bcos\b");
            _keyWords.Add(@"\btan\b");
            _keyWords.Add(@"\basin\b");
            _keyWords.Add(@"\bacos\b");
            _keyWords.Add(@"\batan\b");
            _keyWords.Add(@"\bmax\b");
            _keyWords.Add(@"\bmin\b");

            _keyWords.Add(@"\bnot\b");
            _keyWords.Add(@"\band\b");
            _keyWords.Add(@"\bnull\?\b");
            _keyWords.Add(@"\bexists\?\b");
            _keyWords.Add(@"\bint\b");
            _keyWords.Add(@"\bdouble\b");
            _keyWords.Add(@"\blist\b");
            _keyWords.Add(@"\btype\b");
            _keyWords.Add(@"\bstring\b");

            _keyWords.Add(@"\bchr\b");
            _keyWords.Add(@"\bsubstring\b");

            _keyWords.Add(@"\bappend\b");
            _keyWords.Add(@"\bapply\b");
            _keyWords.Add(@"\bfold\b");
            _keyWords.Add(@"\bmap\b");
            _keyWords.Add(@"\bfilter\b");
            _keyWords.Add(@"\bpush-back!\b");
            _keyWords.Add(@"\bpop-back!\b");
            _keyWords.Add(@"\bnth\b");
            _keyWords.Add(@"\blength\b");
            _keyWords.Add(@"\bcons\b");
            _keyWords.Add(@"\bcar\b");
            _keyWords.Add(@"\bcdr\b");

            // Types
            _identiferTypes.Add(@"\bdump\b");
            _identiferTypes.Add(@"\bread-line\b");
            _identiferTypes.Add(@"\bload\b");
            _identiferTypes.Add(@"\bopen\b");
            _identiferTypes.Add(@"\bclose\b");
            _identiferTypes.Add(@"\bread-file\b");
            _identiferTypes.Add(@"\bwrite-file\b");
            _identiferTypes.Add(@"\bprint\b");
            _identiferTypes.Add(@"\bprintln\b");
            _identiferTypes.Add(@"\bexit\b");
            _identiferTypes.Add(@"\bsystem\b");
            _identiferTypes.Add(@"\bgetenv\b");
        }

        #endregion
    };
}