﻿//------------------------------------------------------------------------------
// <copyright file="ZiSyntaxClassificationDefinition.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace ZiLang
{
    /// <summary>
    /// Classification type definition export for ZiSyntax
    /// </summary>
    internal static class ZiSyntaxClassificationDefinition
    {
        // This disables "The field is never used" compiler's warning. Justification: the field is used by MEF.
#pragma warning disable 169

        /// <summary>
        /// Defines the "ZiSyntax" classification type.
        /// </summary>
        [Export(typeof(ClassificationTypeDefinition))]
        [Name("ZiSyntax")]
        private static ClassificationTypeDefinition typeDefinition;

        [Export(typeof(ClassificationTypeDefinition))]
        [Name("ZiLangNumeric")]
        internal static ClassificationTypeDefinition ZiLangNumericType = null;

        [Export]
        [Name("zil")]
        [BaseDefinition("text")]
        internal static ContentTypeDefinition ZiLangContentDefinition = null;

        [Export]
        [FileExtension(".zil")]
        [ContentType("zil")]
        internal static FileExtensionToContentTypeDefinition
                  ZiLangFileExtensionDefinition = null;
#pragma warning restore 169
    }
}
