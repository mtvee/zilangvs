﻿//------------------------------------------------------------------------------
// <copyright file="ZiSyntaxProvider.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;
using Microsoft.VisualStudio.Language.StandardClassification;
using System.Text.RegularExpressions;

namespace ZiLang
{
    /// <summary>
    /// Classifier provider. It adds the classifier to the set of classifiers.
    /// </summary>
    [Export(typeof(IClassifierProvider))]
    [ContentType("zil")] // This classifier applies to all text files.
    internal class ZiSyntaxProvider : IClassifierProvider
    {
        // Disable "Field is never assigned to..." compiler's warning. Justification: the field is assigned by MEF.
#pragma warning disable 649

        /// <summary>
        /// Classification registry to be used for getting a reference
        /// to the custom classification type later.
        /// </summary>
        [Import]
        //private IClassificationTypeRegistryService classificationRegistry;
        internal IClassificationTypeRegistryService ClassificationRegistry = null;

        public IClassifier GetClassifier(ITextBuffer buffer)
        {
            return buffer.Properties.GetOrCreateSingletonProperty<ZiLang>(delegate { return new ZiLang(ClassificationRegistry); });
        }
#pragma warning restore 649

    }

    #region Classifier
    /// <summary>
    /// Classifier that classifies all text as an instance of the OrinaryClassifierType
    /// </summary>
    class ZiLang : IClassifier
    {
        ZiLangLanguage _ziLangLanguage;
        IClassificationType _classificationType;
        IClassificationType _whiteSpaceType;
        IClassificationType _keywordType;
        IClassificationType _commentType;
        IClassificationType _stringType;
        IClassificationType _identifierType;
        IClassificationType _numericType;

        internal ZiLang(IClassificationTypeRegistryService registry)
        {
            _ziLangLanguage = new ZiLangLanguage();
            _classificationType = registry.GetClassificationType("ZiLang");
            _whiteSpaceType = registry.GetClassificationType(
                          PredefinedClassificationTypeNames.WhiteSpace);
            _keywordType = registry.GetClassificationType(
                          PredefinedClassificationTypeNames.Keyword);
            _commentType = registry.GetClassificationType(
                          PredefinedClassificationTypeNames.Comment);
            _stringType = registry.GetClassificationType(
                          PredefinedClassificationTypeNames.String);
            _identifierType = registry.GetClassificationType(
                          PredefinedClassificationTypeNames.Operator);
            _numericType = registry.GetClassificationType("BrightScriptNumeric");
        }

        /// <summary>
        /// This method scans the given SnapshotSpan for potential matches for this
        /// classification.
        /// In this instance, it classifies everything and returns each span as a
        /// new ClassificationSpan.
        /// </summary>
        /// <param name="trackingSpan">The span currently being classified</param>
        /// <returns>A list of ClassificationSpans that represent spans identified
        /// to be of this classification</returns>
        public IList<ClassificationSpan> GetClassificationSpans(SnapshotSpan span)
        {
            //create a list to hold the results
            List<ClassificationSpan> classifications = new List<ClassificationSpan>();
            string current = span.GetText();
            bool commentFound = false;
            // Note:  Comments should go to the end of the line.
            foreach (var item in _ziLangLanguage.Comments)
            {
                Regex reg = new Regex(item, RegexOptions.IgnoreCase);
                var matches = reg.Matches(current);
                for (int i = 0; i < matches.Count; i++)
                {
                    commentFound = true;
                    Match m = matches[i];
                    Span new_span = new Span(span.Start.Position + m.Index,
                                          current.Length - m.Index);
                    SnapshotSpan new_snapshot = new SnapshotSpan(span.Snapshot,
                                          new_span);
                    var newText = new_snapshot.GetText();
                    classifications.Add(new ClassificationSpan(new_snapshot,
                                          _commentType));
                }
            }
            if (commentFound)
                return classifications;
            Classify(classifications, current, span, _ziLangLanguage.Custom,
                          _classificationType);
            Classify(classifications, current, span, _ziLangLanguage.Quoted,
                          _stringType);
            Classify(classifications, current, span, _ziLangLanguage.KeyWords,
                          _keywordType);
            Classify(classifications, current, span, _ziLangLanguage.IdentifierTypes,
                          _identifierType);
            Classify(classifications, current, span, _ziLangLanguage.Numeric,
                          _numericType);
            return classifications;
        }
        private void Classify(List<ClassificationSpan> classifications, string current,
                  SnapshotSpan span, List<string> matchList, IClassificationType classificationType)
        {
            foreach (var item in matchList)
            {
                Regex reg = new Regex(item, RegexOptions.IgnoreCase);
                var matches = reg.Matches(current);
                for (int i = 0; i < matches.Count; i++)
                {
                    Match m = matches[i];
                    Span new_span = new Span(span.Start.Position + m.Index, m.Length);
                    SnapshotSpan new_snapshot = new SnapshotSpan(span.Snapshot, new_span);
                    var newText = new_snapshot.GetText();
                    if( classificationType != null)
                        classifications.Add(new ClassificationSpan(new_snapshot,
                                              classificationType));
                }
            }
        }

#pragma warning disable 67
        // This event gets raised if a non-text change would affect the classification
        // in some way, for example typing /* would cause the classification to
        // change in C# without directly affecting the span.
        public event EventHandler<ClassificationChangedEventArgs> ClassificationChanged;
#pragma warning restore 67
    }
}
#endregion
