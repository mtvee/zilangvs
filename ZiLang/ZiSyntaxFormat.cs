﻿//------------------------------------------------------------------------------
// <copyright file="ZiSyntaxFormat.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System.ComponentModel.Composition;
using System.Windows.Media;
using Microsoft.VisualStudio.Text.Classification;
using Microsoft.VisualStudio.Utilities;

namespace ZiLang
{
    /// <summary>
    /// Defines an editor format for the ZiSyntax type that has a purple background
    /// and is underlined.
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "ZiSyntax")]
    [Name("ZiSyntax")]
    [UserVisible(true)] // This should be visible to the end user
    [Order(Before = Priority.Default)] // Set the priority to be after the default classifiers
    internal sealed class ZiSyntaxFormat : ClassificationFormatDefinition
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ZiSyntaxFormat"/> class.
        /// </summary>
        public ZiSyntaxFormat()
        {
            this.DisplayName = "ZiSyntax"; // Human readable version of the name
            this.BackgroundColor = Colors.BlueViolet;
            //this.TextDecorations = System.Windows.TextDecorations.Underline;
        }
    }

    #region Format definition
    /// <summary>
    /// Defines an editor format for the BrightScript type that has a purple background
    /// and is underlined.
    /// </summary>
    [Export(typeof(EditorFormatDefinition))]
    [ClassificationType(ClassificationTypeNames = "ZiLangNumeric")]
    [Name("ZiLangNumeric")]
    [UserVisible(true)] //this should be visible to the end user
    [Order(Before = Priority.Default)] //set the priority to be after the default classifiers
    internal sealed class ZiLangNumericFormat : ClassificationFormatDefinition
    {
        /// <summary>
        /// Defines the visual format for the "BrightScript" classification type
        /// </summary>
        public ZiLangNumericFormat()
        {
            this.DisplayName = "ZiLangNumeric"; //human readable version of the name
            this.ForegroundColor = Colors.Goldenrod;
        }
    }
    #endregion
}
